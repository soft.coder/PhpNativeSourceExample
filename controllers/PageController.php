<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PageController
 *
 * @author Serega
 */
header("Content-type:text/html; charset=utf-8");

$CONFIG = parse_ini_file(dirname(__FILE__) . "/../config.ini", true);

require_once dirname(__FILE__) . "/../lib/Model.php";
require_once dirname(__FILE__) . "/../modules/auth/init.php";
 
class PageController {
    private $charset, $title, $cssStyles, $jsScripts, $isObStartEnabled;
    function __construct() {
        $this->charset = "utf-8";
        $this->title = "Default Page";
        $this->cssStyles = array();
        $this->jsScripts = array();
        $this->isObStartEnabled = false;
    }
    /**
     * @param string $title
     */
    function setTitle($title){
        $this->title = htmlspecialchars($title);
    }
    /**
     * @return string
     */
    function getTitle(){
        return $this->title;
    }
    /**
     * @param string $charset
     */
    function setCharset($charset){
        $this->charset =htmlspecialchars($charset);
    }
    /**
     * @return string
     */
    function getCharset(){
        return $this->charset;
    }
    /**
     * @param string $cssUrl
     */
    function addCssStyle($cssUrl){
        $this->cssStyles[] = htmlspecialchars($cssUrl);
    }
    /**
     * @return array
     */
    function getCssStyles(){
        return $this->cssStyles;
    }
    /**
     * @param string $jsUrl
     */
    function addJsScript($jsUrl){
        $this->jsScripts[] = htmlspecialchars($jsUrl);
    }
    /**
     * @return array
     */
    function getJsScripts(){
        return $this->jsScripts;
    }
    /**
     * Turn the output buffering (default off)
     */
    function ObStartEnable(){
        $this->isObStartEnabled = false;
    }
    /**
     * Turn off the output buffering (default off)
     */
    function ObStartDisable(){
        $this->isObStartEnabled = false;
    }
    /**
     * Showing header of page
     */
    function ShowHeader(){
        if($this->isObStartEnabled){
            ob_start();
        }
        include dirname(__FILE__) . "/../views/PageHeaderView.php";
    }
    /**
     * Showing footer of page
     */
    function ShowFooter(){
        include dirname(__FILE__) . "/../views/PageFooterView.php";
    }
}

?>
