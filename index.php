<?php 
    require_once dirname(__FILE__) . "/controllers/PageController.php";
    $Page = new PageController();
    
    require_once dirname(__FILE__) . '/modules/menu/MenuModuleManager.php';
    $menuModuleManager = new MenuModuleManager();    

    $Page->setCharset("utf-8");
    $Page->setTitle("Вход в систему");
    $Page->addCssStyle(AUTH_CSS);
    $Page->addCssStyle($menuModuleManager->getCssUrl());
    $Page->ObStartEnable();
    
    $Page->ShowHeader();
?>
<div>
<?php include AUTH_MENU_PATH; ?>
<?php $menuModuleManager->Show() ?>
</div>
<div  style="width: 104px; margin: 300px auto;">
    <h3>Home page</h3>
</div>
<?php $Page->ShowHeader(); ?>