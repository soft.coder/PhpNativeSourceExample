<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 05.11.13
 * Time: 19:10
 * To change this template use File | Settings | File Templates.
 */

class Model {
    protected $host, $username, $password, $dbName;

    function __construct(){
        //global $CONFIG;
		$CONFIG = parse_ini_file(dirname(__FILE__) . "/../config.ini", true);
        $this->host = $CONFIG["DATABASE"]["host"];
        $this->dbName = $CONFIG["DATABASE"]["databaseName"];
        $this->username = $CONFIG["DATABASE"]["username"];
        $this->password = $CONFIG["DATABASE"]["password"];
    }
    /**
     * @param $query
     * @param $returnResultOrDb
     * @return bool|mysqli_result|mysqli
     */
    protected function Query($query, $returnResultOrDb=true)
    {		
        $db = new mysqli($this->host, $this->username, $this->password, $this->dbName);
        if ($db->connect_errno)
            echo "Connection error [number: {$db->errno}, msg: {$db->error}]";
        if (!($res = $db->query($query)))
            if ($db->errno == 1146) // table is not exists
                $this->InstallDb();
            // cascade can't delete more then 15 level of deep, manually delete
            elseif($db->errno != 1451) //msg: Cannot delete or update a parent row: a foreign key constraint fails
                echo "Query error [number: {$db->errno}, msg: {$db->error}]";
        if ( $returnResultOrDb == true )
            return $res;
        else
            return $db;
    }

}