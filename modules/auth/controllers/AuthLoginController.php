<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 21.10.13
 * Time: 22:55
 * To change this template use File | Settings | File Templates.
 */


require_once dirname(__FILE__)."/../models/AuthModel.php";

class AuthLoginController {

    protected $authLoginViewPath;

    protected $isLoginFailed = false;

    function __construct(){
        $this->authLoginViewPath =  dirname(__FILE__)."/../views/AuthLoginView.php";

        if(!isset($_REQUEST["doLogin"]))
            $this->ShowForTheFirstTimeForm();
        else
            $this->Login();
    }
    protected function Login()
    {
        $model = new AuthModel();
        if ($user = $model->Authenticate($_REQUEST["username"], $_REQUEST["password"]))
        {
            $_SESSION["User"] = $user;
            $loginReferer = $_SESSION["LoginReferer"];
            unset($_SESSION["LoginReferer"]);
            header("Location: $loginReferer");
        }
        else
        {
            $this->isLoginFailed = true;
            require $this->authLoginViewPath;
        }
    }

    protected function ShowForTheFirstTimeForm()
    {
        $this->isLoginFailed = false;
        $_SESSION["LoginReferer"] = $_SERVER["HTTP_REFERER"];
        require $this->authLoginViewPath;
    }
}

$authLoginController = new AuthLoginController();
