<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class AuthMenuController{
    protected $isAuthUser, $isAdmin, $username;
    
    const AUTH_LOGIN_URL = '/modules/auth/controllers/AuthLoginController.php';
    const AUTH_LOGOUT_URL = '/modules/auth/controllers/AuthLogoutController.php';
    const AUTH_ACCESS_DENIED_URL = '/modules/auth/views/AuthAccessDeniedView.php';
    const AUTH_REGISTER_URL = '/modules/auth/controllers/AuthRegisterController.php';
    const AUTH_USERS_URL = '/modules/auth/controllers/AuthUsersController.php';
    
    function __construct() {
        $this->CheckUser();
        $this->Show();
    }
    protected function CheckUser(){
        $this->isAdmin = false;
        if(isset($_SESSION["User"])){
            $this->isAuthUser = true;
            $this->username = $_SESSION["User"]->getUsername();
            if($_SESSION["User"]->getRole() == Role::ADMIN)
                $this->isAdmin = true;
        }
        else
            $this->isAuthUser = false;
    }
    protected function Show(){
        include dirname(__FILE__) . '/../views/AuthMenuView.php';
    }
}
$authMenuController = new AuthMenuController();