<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 02.11.13
 * Time: 13:18
 * To change this template use File | Settings | File Templates.
 */

require_once dirname(__FILE__). "/../models/AuthModel.php";

class AuthRegisterController {

    protected $authRegisterViewPath, $isRegisterFailed, $registerErrorMessage;


    function __construct(){
        $this->authRegisterViewPath = dirname(__FILE__) . "/../views/AuthRegisterView.php";

        if (!isset($_REQUEST["doRegister"]))
            $this->ShowForTheFirstTimeForm();
        else
            $this->Register();

    }
    protected function Register(){
        $username = trim($_REQUEST["username"]);
        $password = trim($_REQUEST["password"]);
        $passwordConfirm = trim($_REQUEST["passwordConfirm"]);
        $user = new User($username, new Role(Role::USER), $password);

        if ( empty($username) == false)
            if  ($password == $passwordConfirm)
            {
                $model = new AuthModel();
                $createdUser = $model->CreateUser($user);
                if ( $createdUser != false )
                {
                    $_SESSION["User"] = $createdUser;
                    $registerReferer = $_SESSION["RegisterReferer"];
                    unset($_SESSION["RegisterReferer"]);
                    header("Location: $registerReferer");
                }
                else
                    $this->registerFailed("Пользователь с таким именем уже существует");
            }
            else
                $this->registerFailed("Пароли не совпадают");
        else
            $this->registerFailed("Не правильное имя пользователя");
    }

    protected function ShowForTheFirstTimeForm()
    {
        $this->isRegisterFailed = false;
        $_SESSION["RegisterReferer"] = $_SERVER["HTTP_REFERER"];
        require $this->authRegisterViewPath;
    }

    /**
     * @param string $msg
     */
    protected function registerFailed($msg)
    {
        $this->isRegisterFailed = true;
        $this->registerErrorMessage = $msg;
        require $this->authRegisterViewPath;
    }
}

$authRegisterController = new AuthRegisterController();