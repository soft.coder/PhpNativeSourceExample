<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 02.11.13
 * Time: 18:18
 * To change this template use File | Settings | File Templates.
 */

require_once dirname(__FILE__) . "/../models/AuthModel.php";

class AuthUsersController {

    protected $users;

    function __construct(){
        if (isset($_SESSION["User"]) && $_SESSION["User"]->getRole() == Role::ADMIN)
        {
            if (isset($_REQUEST["doRemove"], $_REQUEST["username"]) && count($_REQUEST["username"]) > 0)
                $this->removeUsers();
            $this->showUsers();
        }
        else
            include dirname(__FILE__) . "/../views/AuthAccessDeniedView.php";
    }
    function showUsers(){
        $model = new AuthModel();
        $this->users  = $model->GetUsers();
        include dirname(__FILE__) . "/../views/AuthUsersView.php";
    }
    function removeUsers(){
        $model = new AuthModel();;
        $usernames = $_REQUEST["username"];
        foreach($usernames as $username){
            $model->RemoveUser($username);
        }
    }

}

$authUsersController = new AuthUsersController();