<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 01.11.13
 * Time: 16:57
 * To change this template use File | Settings | File Templates.
 */

class Role{
    protected $roleId;
    const ADMIN = "admin";
    const MODERATOR = "moderator";
    const USER = "user";
    function __construct($roleConst){
        switch($roleConst){
            case self::ADMIN:
                $this->roleId = 0;
                break;
            case self::MODERATOR:
                $this->roleId = 1;
                break;
            case self::USER:
                $this->roleId = 2;
                break;
            default:
                throw new Exception("invalid role");
        }
    }
    function getRole(){
        switch($this->roleId){
            case 0:
                return self::ADMIN;
            case 1:
                return self::MODERATOR;
            case 2:
                return self::USER;
            default:
                throw new Exception("invalid role");
        }
    }
    function __toString(){
        return $this->getRole();
    }
}
