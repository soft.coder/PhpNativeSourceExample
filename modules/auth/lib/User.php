<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 22.10.13
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */

require_once dirname(__FILE__) . "/Role.php";

class User {

    protected $id, $username, $password, $role;
    public function __construct($username, Role $role, $password=null, $id=null){
        $this->username = $username;
        $this->role = $role;
        $this->password = $password;
        $this->id = $id;
    }
    public function getId(){
        return $this->id;
    }
    public function getUsername(){
        return $this->username;
    }
    public function getPassword(){
        return $this->password;
    }
    public function getRole(){
        return $this->role->__toString();
    }
    public function deletePasswrod(){
        $this->password = null;
    }
}