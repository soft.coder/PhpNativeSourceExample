<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 21.10.13
 * Time: 22:55
 * To change this template use File | Settings | File Templates.
 */

require_once dirname(__FILE__)."/../lib/User.php";
require_once dirname(__FILE__) . '/../../../lib/Model.php';

class AuthModel extends Model {
    const  DEFAULT_ADMIN_USERNAME = "admin";
    const  DEFAULT_ADMIN_PASSWORD = "123";

    const TABLE_PREFIX = "auth_";

    const TABLE_USERS = "users";

    /**
     * @param string $username
     * @param string $password
     * @return User|bool
     */
    public function Authenticate($username, $password)
    {
        $tableName = self::TABLE_PREFIX . self::TABLE_USERS;
        $passwordHash = sha1($password);
        if(!get_magic_quotes_gpc())
            $username = addslashes($username);

        $query = "SELECT id, username, role FROM $tableName
                        WHERE username = '$username' AND password = '$passwordHash'";
        $res = $this->Query($query);

        if ( $res->num_rows > 0 ){
            $row = $res->fetch_assoc();

            return new User($row["username"], new Role($row["role"]), null, $row["id"]);
        }
        else
            return false;
    }
    /**
     * @param User $user
     * @return int
     */
    public function CreateUser(User $user)
    {
        $tableName = self::TABLE_PREFIX . self::TABLE_USERS;
        if ( !get_magic_quotes_gpc() )
            $username = addslashes($user->getUsername());
        else
            $username = $user->getUsername();

        $role = $user->getRole();
        $hashOfPassword = sha1($user->getPassword());
        $query = "INSERT INTO $tableName (username, password, role)
                        VALUES ('$username', '$hashOfPassword', '$role')";
        $db = $this->Query($query, false);
        if ( $db->affected_rows > 0 ){
            $queryGetCreatedUser = "SELECT id, username, role FROM $tableName WHERE username='$username'";
            $res = $this->Query($queryGetCreatedUser);
            if ( $res->num_rows > 0 ){
                $row = $res->fetch_assoc();
                $user = new User($row["username"], new Role($row["role"]), null, $row["id"]);
                return $user;
            }
        }
        else
            return false;
    }

    /**
     * @param string $username
     * @return int affected rows
     */
    public function RemoveUser($username)
    {
        $tableName = self::TABLE_PREFIX . self::TABLE_USERS;

        if(!get_magic_quotes_gpc())
            $username = addslashes($username);

        $query = "DELETE FROM $tableName WHERE username='$username'";
        $db = $this->Query($query, false);
        return $db->affected_rows;
    }
    /**
     * @return array
     */
    public function GetUsers()
    {
        $tableName = self::TABLE_PREFIX . self::TABLE_USERS;
        $query = "SELECT id, username, role FROM $tableName WHERE username != '". self::DEFAULT_ADMIN_USERNAME . "'";
        $res = $this->Query($query);
        $users = array();
        for($i=0; $i < $res->num_rows; $i++)
        {
            $row = $res->fetch_assoc();
            $username = stripslashes($row["username"]);
            $id = $row["id"];
            $role = new Role($row["role"]);
            $user = new User($username, $role, null, $id);
            $users[] = $user;
        }
        return $users;

    }
    protected function InstallDb()
    {
        $tableName = self::TABLE_PREFIX . self::TABLE_USERS;
        $query = <<<EOD
            CREATE TABLE $tableName (
                id INT AUTO_INCREMENT PRIMARY KEY,
                username CHAR(50) NOT NULL UNIQUE,
                password CHAR(100) NOT NULL,
                role ENUM('admin', 'moderator', 'user') NOT NULL,
                INDEX (username)
            )ENGINE=InnoDb;
EOD;
        $adminUsername = self::DEFAULT_ADMIN_USERNAME;
        $adminHashPassword = sha1(self::DEFAULT_ADMIN_PASSWORD);
        $adminRole = Role::ADMIN;
        $queryInsertAdmin = "INSERT INTO $tableName (username, password, role)
                                VALUES ('$adminUsername', '$adminHashPassword', '$adminRole')";

        $db = new mysqli($this->host, $this->username, $this->password, $this->dbName);
        if(!$db->connect_error)
            echo "Connection error [number: {$db->errno}, msg: {$db->error}]";

        if ( !$db->query($query) )
            echo "Table error [number: {$db->errno}, msg: {$db->error}]";
        if ( !$db->query($queryInsertAdmin) )
            echo "Insert default admin error [number: {$db->errno}, msg: {$db->error}]";
    }
}