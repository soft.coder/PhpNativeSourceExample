<?php 
    require_once dirname(__FILE__) . "/../../../controllers/PageController.php";
    $Page = new PageController();
    
    require_once dirname(__FILE__) . '/../../menu/MenuModuleManager.php';
    $menuModuleManager = new MenuModuleManager();    
    

    $Page->setCharset("utf-8");
    $Page->setTitle("Доступ запрещен");
    $Page->addCssStyle(AUTH_CSS);
    $Page->addCssStyle($menuModuleManager->getCssUrl());
    $Page->ObStartEnable();
    
    $Page->ShowHeader();
?>
<div>
<?php include AUTH_MENU_PATH; ?>
<?php $menuModuleManager->Show() ?>
</div>
<div class="auth accessdenied">
    <h4 class="title">Доступ запрeщен</h4>
    <p style="text-align: center; "><a href="<?php echo AuthMenuController::AUTH_LOGIN_URL ?>">Войти</a></p>
</div>
<?php $Page->ShowFooter() ?>