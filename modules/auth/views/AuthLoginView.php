<?php 
    require_once dirname(__FILE__) . "/../../../controllers/PageController.php";
    $Page = new PageController();
    
    require_once dirname(__FILE__) . '/../../menu/MenuModuleManager.php';
    $menuModuleManager = new MenuModuleManager();    

    $Page->setCharset("utf-8");
    $Page->setTitle("Вход в систему");
    $Page->addCssStyle(AUTH_CSS);
    $Page->addCssStyle($menuModuleManager->getCssUrl());
    $Page->ObStartEnable();
    
    $Page->ShowHeader();
?>
<div>
<?php include AUTH_MENU_PATH; ?>
<?php $menuModuleManager->Show() ?>
</div>
<div class="auth login">
    <h3 class="title">Вход в систему</h3>
    <form method="post" action="<?php '../controllers/AuthLoginController.php'?>">
        <?php if( $this->isLoginFailed == true ): ?>
            <p><span class="unsucces">Неверное имя пользователся и/или пароль</span></p>
        <?php endif; ?>
        <p>Имя пользователя:</p>
        <input class="username" name="username" type="text"/>
        <p>Пароль:</p>
        <input class="password" name="password" type="password"/>
        <p class="buttons"><input type="submit" name="doLogin" value="Войти"/></p>
    </form>
</div>
<?php $Page->ShowFooter() ?>
