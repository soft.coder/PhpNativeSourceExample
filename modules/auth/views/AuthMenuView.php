<div class="auth-menu">
    <p>
        <?php if($this->isAuthUser == false): ?>
            <a href="<?php echo self::AUTH_REGISTER_URL ?>">
                Регистрация
            </a>&nbsp;
        <?php else: ?>
            <span><?php echo $this->username ?> | </span>
        <?php endif;?>

        <?php if($this->isAuthUser && $this->isAdmin): ?>
            <a href="<?php echo self::AUTH_USERS_URL ?>">Пользователи</a>&nbsp;
        <?php endif;?>
        <a href="<?php echo $this->isAuthUser ? self::AUTH_LOGOUT_URL : self::AUTH_LOGIN_URL ?>">
            <?php echo $this->isAuthUser ? "Выйти" : "Войти"?>
        </a>
    </p>
</div>