<?php 
    require_once dirname(__FILE__) . "/../../../controllers/PageController.php";
    $Page = new PageController();
    
    require_once dirname(__FILE__) . '/../../menu/MenuModuleManager.php';
    $menuModuleManager = new MenuModuleManager();    

    $Page->setCharset("utf-8");
    $Page->setTitle("Регистрация в системе");
    $Page->addCssStyle(AUTH_CSS);
    $Page->addCssStyle($menuModuleManager->getCssUrl());
    $Page->ObStartEnable();
    
    
    $Page->ShowHeader();
?>
<div>
<?php include AUTH_MENU_PATH; ?>
<?php $menuModuleManager->Show() ?>
</div>
<div class="auth register">
    <h3 class="title">Вход в систему</h3>
    <form method="post" action="<?php '../controllers/AuthLoginController.php'?>">
        <?php if( $this->isRegisterFailed == true ): ?>
            <p><span class="unsucces"><?php echo $this->registerErrorMessage ?></span></p>
        <?php endif; ?>
        <p>Имя пользователя:</p>
        <input class="username" name="username" type="text" />
        <p>Пароль:</p>
        <input class="password" name="password" type="password"/>
        <p>Повторный ввод пароля:</p>
        <input class="password" name="passwordConfirm" type="password"/>
        <p class="buttons"><input type="submit" name="doRegister" value="Зарегистрироваться"/></p>
    </form>
</div>
<?php $Page->ShowFooter() ?>