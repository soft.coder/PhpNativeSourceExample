<?php 
    require_once dirname(__FILE__) . "/../../../controllers/PageController.php";
    $Page = new PageController();
    
    require_once dirname(__FILE__) . '/../../menu/MenuModuleManager.php';
    $menuModuleManager = new MenuModuleManager();    

    $Page->setCharset("utf-8");
    $Page->setTitle("Пользователи");
    $Page->addCssStyle(AUTH_CSS);
    $Page->addCssStyle($menuModuleManager->getCssUrl());
    $Page->ObStartEnable();
    
    
    $Page->ShowHeader();
?>
<div>
<?php include AUTH_MENU_PATH; ?>
<?php $menuModuleManager->Show() ?>
</div>
<div class="auth users">
    <h3 class="title">Пользователи</h3>
    <form method="post" action="<?php '../controllers/AuthLoginController.php'?>">
        <table class="list">
            <thead>
                <tr>
                    <th>&Xi;</th>
                    <th>ID</th>
                    <th>Имя пользователя</th>
                    <th>Роль</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($this->users as $user): ?>
                <tr>
                    <td align="center"><input type="checkbox" id="<?php echo $user->getUsername() ?>" name="username[]" value="<?php echo $user->getUsername() ?>" /> </td>
                    <td align="center"><label for="<?php echo $user->getUsername() ?>"><div><?php echo $user->getId() ?></div></label></td>
                    <td><label for="<?php echo $user->getUsername() ?>"><div><?php echo $user->getUsername() ?></div></label></td>
                    <td><label for="<?php echo $user->getUsername() ?>"><div><?php echo $user->getRole() ?></div></label></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p class="buttons"><input type="submit" name="doRemove" value="Удалить"/></p>
    </form>
</div>
<?php $Page->ShowFooter() ?>