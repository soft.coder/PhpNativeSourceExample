<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 05.11.13
 * Time: 21:16
 * To change this template use File | Settings | File Templates.
 */

require_once dirname(__FILE__) . "/../lib/Comment.php";
require_once dirname(__FILE__) . "/../models/CommentModel.php";

class CommentAddController {
    protected $isValidCommentData;
    function __construct(){
        if (isset($_SESSION["User"])){
            if (isset($_REQUEST["doAdd"], $_REQUEST["message"]) && trim($_REQUEST["message"]) &&
                    strlen(trim($_REQUEST["message"]) <= 1000)){
                $msg = trim($_REQUEST["message"]);
                $comment = $this->Add($msg);
                if($comment != false)
                    $this->SelfRedirectToComment($comment->getId());
                else
                    $this->SelfRedirect();
            }
            $this->ShowForm();
        }
    }

    /**
     * @param int $comment_id
     */
    function SelfRedirectToComment($comment_id){
        header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}#comment_{$comment_id}");
    }
    function SelfRedirect(){
        header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}");
    }
    function Add($msg){
        $model = new CommentModel();
        $commentAdded = $model->AddComment($_SESSION["User"]->getId(), $msg);
        $_SESSION["commentAdded_id"] = $commentAdded->getId();
        return $commentAdded;
    }
    function ShowForm(){
        include dirname(__FILE__) .  "/../views/CommentAddView.php";
    }

}
$commentAddController = new CommentAddController();