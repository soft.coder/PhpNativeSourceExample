<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 07.11.13
 * Time: 14:58
 * To change this template use File | Settings | File Templates.
 */

require_once dirname(__FILE__) . '/../../auth/lib/Role.php';
require_once dirname(__FILE__) . '/../lib/Comment.php';
require_once dirname(__FILE__) . '/../models/CommentModel.php';
require_once dirname(__FILE__) . '/../controllers/CommentShowController.php';

class CommentManageController {

    protected $isUserAdmin;


    public function __construct(){
        if(isset($_SESSION["User"])){
            if($_SESSION["User"]->getRole() == Role::ADMIN)
                $this->isUserAdmin = true;
            else
                $this->isUserAdmin = false;

            if(isset($_REQUEST["doReply"]))
                $this->RequestCheckReply();
            elseif(isset($_REQUEST["doRemove"]))
                $this->RequestCheckRemove();
            elseif(isset($_REQUEST["doRemoveBranch"]))
                $this->RequestCheckRemoveBranch();
        }
    }
    protected function RequestCheckReply(){
        if(isset($_REQUEST["doReply"], $_REQUEST["id"], $_REQUEST["message"]) &&
            intval($_REQUEST["id"]) && trim($_REQUEST["message"]) && strlen(trim($_REQUEST["message"])) <= 1000){
            $message = trim($_REQUEST["message"]);
            $id = intval($_REQUEST['id']);
            $user_id = $_SESSION['User']->getId();
            $this->Reply($id, $message, $user_id);
        }
    }
    protected function RequestCheckRemove(){
        if(isset($_REQUEST["doRemove"], $_REQUEST["id"]) && intval($_REQUEST["id"])){
            $id = intval($_REQUEST["id"]);
            if ( isset($_REQUEST["parent_id"]) && intval($_REQUEST["parent_id"]))
                $parent_id = intval($_REQUEST["parent_id"]);
            else
                $parent_id = null;
            $this->Remove($id, $parent_id);
        }
    }
    protected function RequestCheckRemoveBranch(){
        if(isset($_REQUEST["doRemoveBranch"], $_REQUEST["id"]) && intval($_REQUEST["id"])){
            $id = $_REQUEST["id"];
            $this->RemoveBranch($id);
        }
    }

    /**
     * @param Comment $comment
     */
    public function Show(Comment $comment){
        if(isset($_SESSION["User"])){

            if($_SESSION["User"]->getId() == $comment->getUser()->getId())
                $isCanRemove = true;
            else
                $isCanRemove = false;

            if(isset($_REQUEST["doReply"], $_REQUEST["id"]) && $_REQUEST["id"] == $comment->getId() &&
                        !isset($_REQUEST["message"],$_REQUEST["doCancel"]))
                $isWriteReply = true;
            else
                $isWriteReply = false;

            if (!is_null($comment->getParentComment()))
                $actionHref = "#comment_{$comment->getParentComment()->getId()}";
            else
                $actionHref = "#comment_{$comment->getId()}";
                
            $formName = "comment_{$comment->getId()}";

            include dirname(__FILE__) . "/../views/CommentManageView.php";
        }
    }
    /**
     * @param int $id
     * @param string $message
     * @param int $user_id
     */
    protected function Reply($id, $message, $user_id){
        $model = new CommentModel();
        $commentAdded = $model->AddComment($user_id, $message, $id);
        if ( $commentAdded !== false ){
            $_SESSION["commentAdded_id"] = $commentAdded->getId();
            $this->SelfRedirectToComment($id);
        }
        else
            $this->SelfRedirect();
    }

    /**
     * @param int $comment_id
     */
    function SelfRedirectToComment($comment_id){
        header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}#comment_{$comment_id}");
    }
    function SelfRedirect(){
        header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}");
    }

    /**
     * @param int $id
     * @param null|int $parent_id
     */
    protected function Remove($id, $parent_id=null){
        $id = intval($id);
        $model = new CommentModel();
        $model->RemoveComment($id);
        if(!is_null($parent_id))
            $this->SelfRedirectToComment($parent_id);
        else
            $this->SelfRedirect();
    }

    /**
     * @param int $id
     */
    protected function RemoveBranch($id){
        $model = new CommentModel();
        $model->RemoveBranch($id);
        $this->SelfRedirect();
    }
}




