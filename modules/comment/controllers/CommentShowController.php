<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 05.11.13
 * Time: 21:16
 * To change this template use File | Settings | File Templates.
 */

require_once dirname(__FILE__) . "/../lib/Comment.php";
require_once dirname(__FILE__) . "/../models/CommentModel.php";
require_once "CommentManageController.php";

class CommentShowController {
    protected $commentAdded_id, $commentManageController;
    function __construct(CommentManageController $commentManageController){
        $this->commentManageController = $commentManageController;
        if(isset($_SESSION["commentAdded_id"]))
            $this->commentAdded_id = $_SESSION["commentAdded_id"];

        $comments = $this->getAllComments();
        if(!is_null($comments))
            $this->ShowComments($comments);
    }

    /**
     * @return array|null
     */
    function getAllComments(){
        $model = new CommentModel();
        return $model->GetComments();
    }

    /**
     * @param array $comments
     */
    function ShowComments($comments){
        echo "<div class='comment-list'>";
        foreach($comments as $comment){
            $this->ShowCommentBranch($comment);
        }
        echo "</div>";
    }

    /**
     * @param Comment $comment
     * @param int $level
     */
    function ShowCommentBranch(Comment $comment, $level=1){
        if($comment->getId() == $this->commentAdded_id)
            $isCurrentUserAdded = true;
        else
            $isCurrentUserAdded = false;

        $linkMessageHref = "{$_SERVER["REQUEST_URI"]}#comment_{$comment->getId()}";
        $linkMessageName = "comment_{$comment->getId()}";
        
        include dirname(__FILE__) . "/../views/CommentShowView.php";
        
        $childComments = $comment->getChildComments();
        for($i = 0; $i < count($childComments); $i++){
            if (!is_null($childComments[$i]))
                $this->ShowCommentBranch($childComments[$i], $level + 1);
        }
    }
}
$commentManageController = new CommentManageController();
$commentShowController = new CommentShowController($commentManageController);