<?php 
    require_once dirname(__FILE__) . "/../../controllers/PageController.php";
    $Page = new PageController();
    
    require_once dirname(__FILE__) . '/../menu/MenuModuleManager.php';
    $menuModuleManager = new MenuModuleManager();    

    $Page->setCharset("utf-8");
    $Page->setTitle("Комментарии");
    $Page->addCssStyle("css/comment.css");
    $Page->addCssStyle(AUTH_CSS);
    $Page->addCssStyle($menuModuleManager->getCssUrl());
    $Page->addJsScript("js/comment.js");
    $Page->ObStartEnable();
    
    $Page->ShowHeader();
?>
<div>
<?php include AUTH_MENU_PATH; ?>
<?php $menuModuleManager->Show() ?>
</div>
<div class="my-comment">
    <?php require_once "controllers/CommentAddController.php" ?>
    <!--<hr style="border: 1px dashed #CCC" />-->
    <h3>Комментарии</h3>
    <?php require_once "controllers/CommentShowController.php" ?>
</div>
<?php $Page->ShowFooter(); ?>
