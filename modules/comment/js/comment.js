Lib = {
    EventHandler: {
        /**
         * Use addEventListener and attachEvent. For attachEvent create wrapper
         * that init event parameter for callback like in addEventListener,
         * and save this wrapper for removeAttach
         * @param {Element} element
         * @param {string} nameEvent
         * @param {Funciton} callback
         * @returns {Boolean}
         */
        add: function (element, nameEvent, callback){
            if(!element.addEventListener && !element.attachEvent)
                return false;

            if(element.addEventListener)
                element.addEventListener(nameEvent, callback, false);
            else if (element.attachEvent){ // for IE < 9
                var elementName;
                if(document === element)
                    elementName = "document";
                else
                    elementName = element.uniqueID;
                this[elementName] = {};
                var handlersStorage  = this[elementName][nameEvent] = this[elementName][nameEvent] || [];
                var handler = handlersStorage[handlersStorage.length] = {};
                handler.callback = callback;
                handler.wrapperCallback = function(){
                    var e = window.event;
                    var button = e.button !== 4 ? e.button - 1 : 2;
                    var likeDom2Event = {
                        type: e.type,
                        target: e.srcElement,
                        button: button,
                        clientX : e.clientX,
                        clientY : e.clientY,
                        altKey : e.altKey,
                        ctrlKey : e.ctrKey,
                        shiftKey : e.shiftKey,
                        preventDefault: function(){
                            window.event.returnValue = false;
                        },
                        stopPropagation: function(){
                            window.event.cancelBubble = true;
                        }
                    };
                    callback(likeDom2Event);
                };
                element.attachEvent("on"+nameEvent, handler.wrapperCallback );
            }

            return true;
        },
        /**
         * 
         * @param {Element} element
         * @param {string} nameEvent without prefix on
         * @param {Function} callback
         * @returns {Boolean}
         */
        remove: function(element, nameEvent, callback){
            if(!element.removeEventListener && !element.detachEvent)
                return false;

            if(element.removeEventListener)
                element.removeEventListener(nameEvent, callback, false);
            else if (element.detachEvent){ // for IE < 9
                if(this[element.uniqueID] && this[element.uniqueID][nameEvent]){
                    var handlerStorage = this[element.uniqueID][nameEvent];
                    var removeHandlersIndx = [];
                    for(var i = 0; i < handlerStorage.length; i++){
                        if(handlerStorage[i].callback === callback){
                            removeHandlersIndx.push(i);
                            element.detachEvent("on"+nameEvent, handlerStorage[i].wrapperCallback);
                        }
                    }
                    for(var j = 0; j < removeHandlersIndx.length; j++){
                        handlerStorage.splice(removeHandlersIndx[j] + j, 1);
                    }
                }

            }

            return true;
        },
        /**
         * try register handler for event that generate when document loaded 
         * before images.
         * @param {Function} callback
         * @returns {undefined}
         */
        addOnload: function(callback){
            var ready = false;
            var callbackWrapper = function(event){
                if(ready) return;
                if(event.type === "readystatechange" && document.readyState !== "complete")
                    return;
                
                callback(event);
                ready = true;
            };
            this.add(document, "DOMContentLoaded", callbackWrapper);
            this.add(document, "readystatechange", callbackWrapper);
            this.add(document, "load", callbackWrapper);
        }
    },
    Form : {
        /**
         * get from Form input element ( for dynamic created elements in IE < 7 ) 
         * @param {Form} form
         * @param {string} name
         * @returns {Array}
         */
        getByName: function(form, name){
            var result = [];
            var inputElem = form.elements;
            for(var i=0; i < inputElem.length; i++){
                if(inputElem[i].name && inputElem[i].name === name)
                    result.push(inputElem[i]);
            }
            return result;
        }
    }
};
(function(){
    Comment = {
        /**
         * Remove textarea where writing reply, remove 'cancel' button, display
         * 'remove' button if exists.
         * @param {Form} commentForm
         * @returns {undefined}
         */
        cancelReply: function(commentForm){
            var btnReply = commentForm.doReply || Lib.Form.getByName(commentForm, "doReply")[0];
            var btnCancel = commentForm.doCancel || Lib.Form.getByName(commentForm, "doCancel")[0];
            var btnRemove = commentForm.doRemove || Lib.Form.getByName(commentForm, "doRemove")[0];
            var commentTextarea = commentForm.message || Lib.Form.getByName(commentForm, "message")[0];
            var btnContainer = btnCancel.parentNode;
            Lib.EventHandler.remove(btnCancel, "click", Comment.cancelReplyHandler);

            btnContainer.removeChild(btnCancel);
            commentForm.removeChild(commentTextarea);

            if(btnRemove)
                btnRemove.removeAttribute("style");

            btnReply.setAttribute("type", "button");
            Lib.EventHandler.add(btnReply, "click", Comment.startReplyHandler);
            Lib.EventHandler.remove(commentForm, "submit", Comment.validateTextarea);
        },
        /**
         * Event handler for cancel button, that display when writing reply.
         * Call cancelReply function.
         * @param {Event} event
         * @returns {undefined}
         */
        cancelReplyHandler: function (event){
            var commentForm = event.target.form;

            Comment.cancelReply(commentForm);

            delete Comment.startReplyHandler.prevWritingComment;
            event.preventDefault();
        },
        /**
         * Event handler for first click on button 'reply' for start wrtie reply.
         * Create textarea for writing reply, create 'cancel' button for cancel
         * writing reply, hide 'remove' button if exists.
         * @param {Event} event
         * @returns {undefined}
         */
        startReplyHandler: function (event){
            var btnReply = event.target;
            var commentForm = btnReply.form;

            var textarea = document.createElement("textarea");
            textarea.setAttribute("name", "message");
            textarea.setAttribute("maxlength", "1000");
            commentForm.insertBefore(textarea, commentForm.firstChild);
            textarea.focus();

            var btnCancel = document.createElement("input");
            btnCancel.setAttribute("type", "button");
            btnCancel.setAttribute("value", "Отмена");
            btnCancel.setAttribute("name", "doCancel");        
            btnReply.parentNode.insertBefore(btnCancel, btnReply.nextSibling.nextSibling);
            Lib.EventHandler.add(btnCancel, "click", Comment.cancelReplyHandler);

            if(commentForm.doRemove)
                commentForm.doRemove.style.display = "none";       

            Lib.EventHandler.remove(btnReply, "click", Comment.startReplyHandler);
            Lib.EventHandler.add(commentForm, "submit", Comment.validateTextarea);
            btnReply.setAttribute("type", "submit");

            event.preventDefault();

            if(arguments.callee.prevWritingComment)
                Comment.cancelReply(arguments.callee.prevWritingComment);
            arguments.callee.prevWritingComment = commentForm;
        },
        /**
         * Handler for 'submit' event of form.
         * Validate reply, if reply havn't message or message only contains space
         * charaster validate failed, form not send to server and textarea give
         * class 'error'
         * @param {Event} event
         * @returns {undefined}
         */
        validateTextarea: function (event){
            var textarea = event.target.message || Lib.Form.getByName(event.target, "message")[0];
            var pattern = /^\s*$/;
            if(textarea.value === undefined || pattern.test(textarea.value)){
                textarea.setAttribute("class", "error");
                Lib.EventHandler.add(textarea, "keyup", Comment.writingInTextarea);
                event.preventDefault();
            }  
        },
        /**
         * Handler for 'keyup' event of textarea that contain reply.
         * This handler only setting after validateReply falied, and unsetting when
         * textarea give any not space charaster.
         * @param {Event} event
         * @returns {undefined}
         */
        writingInTextarea: function(event){
            var textarea  = event.target;
            var pattern = /^\s*$/;
            if(textarea.value !== undefined && !pattern.test(textarea.value)){
                textarea.removeAttribute("class");
                Lib.EventHandler.remove(textarea, "keyup", arguments.callee);
            }
        },
        registerListeners: function(){
            var pattern = /^comment_\d+/;
            for(var name in document.forms){
                if(pattern.test(name)){
                    var btnReply = document.forms[name].doReply;                
                    if(Lib.EventHandler.add(btnReply, "click", Comment.startReplyHandler))
                        btnReply.setAttribute("type", "button");
                }
                else if (name === "commentAdd"){
                    Lib.EventHandler.add(document.forms[name], "submit", Comment.validateTextarea);
                }
            }
        }
    };
    //window.onload = Comment.registerListeners;
    Lib.EventHandler.addOnload(Comment.registerListeners);
})();