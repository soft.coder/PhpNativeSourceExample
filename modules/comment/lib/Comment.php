<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 05.11.13
 * Time: 21:24
 * To change this template use File | Settings | File Templates.
 */

class Comment {
    protected $id, $user, $message, $parentComment, $childComments, $timeAdded;

    /**
     * @param User $user
     * @param string $message
     * @param Comment $parentComment
     * @param array $childComments
     * @param int $id
     * @param string $timeAdded
     */
    function __construct(User $user, $message, Comment $parentComment=null, $childComments=null, $id=null, $timeAdded=null){
        $this->id = $id;
        $this->user = $user;
        $this->message = $message;
        $this->parentComment = $parentComment;
        $this->childComments &= is_null($childComments) ? array() : $childComments;
        $this->timeAdded = $timeAdded;
    }
    function getId(){
        return $this->id;
    }
    function getUser(){
        return $this->user;
    }
    function getMessage(){
        return $this->message;
    }
    function getParentComment(){
        return $this->parentComment;
    }
    function getChildComments(){
        return $this->childComments;
    }

    /**
     * @param array $childComments
     */
    function setChildComment($childComments){
        $this->childComments = $childComments;
    }
    function getTimeAdded(){
        return $this->timeAdded;
    }
}