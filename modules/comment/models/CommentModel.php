<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 04.11.13
 * Time: 20:32
 * To change this template use File | Settings | File Templates.
 */

require_once dirname(__FILE__) . "/../../auth/models/AuthModel.php";
require_once dirname(__FILE__) . "/../lib/Comment.php";

class CommentModel extends Model {

    const TABLE_NAME = "comment";
    const TABLE_PREFIX = "comment_";

    protected $commentTableName;
    protected $userTableName;

    function __construct(){
        parent::__construct();
        $this->commentTableName = self::TABLE_PREFIX . self::TABLE_NAME;;
        $this->userTableName = AuthModel::TABLE_PREFIX . AuthModel::TABLE_USERS;
    }

    protected function InstallDb(){
        $query = "CREATE TABLE $this->commentTableName (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    user_id INT NOT NULL,
                    parent_id INT,
                    message VARCHAR(1000),
                    time_added TIMESTAMP,
                    INDEX(user_id),
                    INDEX(parent_id),
                    FOREIGN KEY (user_id)
                            REFERENCES $this->userTableName(id)
                            ON UPDATE CASCADE
                            ON DELETE CASCADE,
                    FOREIGN KEY (parent_id)
                        REFERENCES $this->commentTableName(id)
                        ON UPDATE CASCADE
                        ON DELETE CASCADE
                ) ENGINE=InnoDb";
        $this->Query($query, false);
    }

    /**
     * @param int $user_id
     * @param string $message
     * @param int|null $parent_id
     * @return Comment|bool
     */
    function AddComment($user_id, $message, $parent_id=null){
        if(is_null($parent_id))
            $parent_id = "NULL";
        $message = addslashes($message);
        $parent_id = addslashes($parent_id);
        $user_id = addslashes($user_id);
        $query = "INSERT INTO $this->commentTableName (user_id, parent_id, message)
                    VALUES ($user_id, $parent_id , '$message')";
        $db = $this->Query($query, false);

        if ($db->affected_rows > 0){
            $id = $db->insert_id;
            $comment = $this->GetComment($id);
            return $comment;
        }
        return false;
    }

    /**
     * @param int $id
     * @return bool
     */
    function RemoveComment($id){
        $countOfChild = $this->CountOfChild($id);
        if($countOfChild > 0){
            $query = "UPDATE {$this->commentTableName} SET message=NULL WHERE id=$id";
        }
        else
            $query = "DELETE FROM {$this->commentTableName} WHERE id=$id";
        $db = $this->Query($query, false);
        if($db->affected_rows > 0)
            return true;
        else
            return false;
    }

    /**
     * @param $id
     * @return int
     */
    function RemoveBranch($id){
        $countOfChild = $this->CountOfChild($id);
        $query = "DELETE FROM {$this->commentTableName} WHERE id=$id";
        $db = $this->Query($query, false);
        if($db->affected_rows > 0)
            return $db->affected_rows;
        else
            if($countOfChild > 0)
                return $this->RemoveCascadeDescendantsManually($id);
            else
                return 0;
    }

    /**
     * @param int $id
     * @return int
     */
    protected function RemoveCascadeDescendantsManually($id){
        $query = "SELECT id FROM {$this->commentTableName} WHERE parent_id=$id";
        $res = $this->Query($query);
        for($i=0; $i < $res->num_rows; $i++){
            $row = $res->fetch_assoc();
            $childId = $row["id"];
            $affectedRowsFromDescendants = $this->RemoveCascadeDescendantsManually($childId);
            $queryRemove = "DELETE FROM {$this->commentTableName} WHERE id=$childId";
            $db = $this->Query($queryRemove, false);
            return $db->affected_rows + $affectedRowsFromDescendants;
        }
        return 0;
    }
    /**
     * @param int $id
     * @return int
     */
    protected function CountOfChild($id){
        $queryCountOfChild = "SELECT COUNT(*) FROM {$this->commentTableName} WHERE parent_id=$id";
        $resCountOfChild = $this->Query($queryCountOfChild);
        $rowCountOfChild = $resCountOfChild->fetch_row();
        $countOfChild = $rowCountOfChild[0];
        return $countOfChild;
    }

    /**
     * @param int $id
     * @param bool $getParentsComments
     * @param bool $getChildComments
     * @return bool|Comment
     */
    function GetComment($id, $getParentsComments=false, $getChildComments=false){
        $query = "SELECT c.id, c.user_id, c.parent_id, u.username, u.role AS role, c.message, DATE_FORMAT(c.time_added, '%T %d.%m.%Y') as time_added
                        FROM $this->commentTableName c
                        INNER JOIN $this->userTableName u ON c.user_id=u.id WHERE c.id=$id";
        $res = $this->Query($query);
        if($res->num_rows > 0){
            $row = $res->fetch_assoc();

            $username = stripslashes($row["username"]);
            $role = new Role($row["role"]);
            $user_id = $row["user_id"];
            $user = new User($username, $role, null, $user_id);

            $parentComment = null;
            if($getParentsComments == true)
                $parentComment = $this->GetComment($row["parent_id"], true);
            $childComments = null;
            if($getChildComments==true)
                $childComments = $this->GetCommentBranch($row["id"]);

            $message = stripslashes($row["message"]);
            $id = $row["id"];
            $timeAdded = $row["time_added"];
            $comment = new Comment($user, $message, $parentComment, $childComments, $id, $timeAdded );
            return $comment;
        }
        return false;
    }

    /**
     * @return array|null
     */
    function GetComments(){
        $query = "SELECT c.id, c.user_id, u.username, u.role, c.message, DATE_FORMAT(c.time_added, '%T %d.%m.%Y') as time_added
                        FROM $this->commentTableName c
                        INNER JOIN $this->userTableName u ON c.user_id=u.id WHERE c.parent_id IS NULL
                          ORDER BY c.time_added";
        $res = $this->Query($query);
        $comments = null;
        for($i=0; $i< $res->num_rows; $i++){
            $row = $res->fetch_assoc();
            $user = new User(stripslashes($row["username"]), new Role($row["role"]), null, $row["user_id"]);
            $comment = new Comment($user, stripslashes($row["message"]), null, null, $row["id"], $row["time_added"] );
            $commentsBranch  = $this->GetCommentBranch($comment);
            $comment->setChildComment($commentsBranch);
            $comments[] = $comment;
        }
        return $comments;
    }

    /**
     * @param Comment $parentComment
     * @param int $level
     * @return array|null
     */
    protected function GetCommentBranch(Comment $parentComment, $level=1){
        $query = "SELECT c.id, c.user_id, u.username, u.role, c.message, DATE_FORMAT(c.time_added, '%T %d.%m.%Y') AS time_added
                        FROM $this->commentTableName c
                        INNER JOIN $this->userTableName u ON c.user_id=u.id WHERE c.parent_id={$parentComment->getId()}
                          ORDER BY c.time_added";
        $res = $this->Query($query);
        $comments = null;
        for($i=0; $i < $res->num_rows; $i++){
            $row = $res->fetch_assoc();
            $user = new User(stripslashes($row["username"]), new Role($row["role"]), null, $row["user_id"]);
            $comment = new Comment($user, stripslashes($row["message"]), $parentComment, null, $row["id"], $row["time_added"]);
            $commentsBranch = $this->GetCommentBranch($comment, $level + 1);
            $comment->setChildComment($commentsBranch);
            $comments[] = $comment;
        }
        return $comments;
    }
}
$commentModel = new CommentModel();
