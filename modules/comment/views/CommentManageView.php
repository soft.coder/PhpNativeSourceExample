<form action="<?php echo  $actionHref; ?>" name="<?php echo $formName; ?>" method="post">
    <?php if ($isWriteReply == true) : ?>
        <textarea name="message" maxlength="1000"></textarea>
    <?php endif; ?>
    <p class="buttons">
        <input type="submit" name="doReply" value="Ответить" />&nbsp;
        <?php if ($isWriteReply == true) : ?>
            <input type="submit" name="doCancel" value="Отмена" />&nbsp;
        <?php endif; ?>
        <?php if(($isCanRemove == true || $this->isUserAdmin == true) && $isWriteReply == false): ?>
            <input type="submit" name="doRemove" value="Удалить"/>
        <?php endif; ?>
        <?php if($this->isUserAdmin == true && $isWriteReply == false): ?>
            <input type="submit" name="doRemoveBranch" value="Удалить ветку"  />
        <?php endif; ?>
    </p>
    <input type="hidden" name="id" value="<?php echo $comment->getId(); ?>"/>
    <?php if( is_null($comment->getParentComment()) == false ): ?>
        <input type="hidden" name="parent_id" value="<?php echo $comment->getParentComment()->getId(); ?>"/>
    <?php endif; ?>
</form>