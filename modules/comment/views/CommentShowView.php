<div class="comment-show<?php if($isCurrentUserAdded == true) echo " added" ?>" style="margin-left:<?php echo $level <= 20 ? ($level - 1)*10 : 200 ?>px;">
    <div class="title">
        <div class="timeadd"><p><?php echo htmlspecialchars($comment->getTimeAdded()); ?></p></div>
        <div class="control">
            <p>
                <span class="username"><?php echo htmlspecialchars($comment->getUser()->getUsername()) ?></span>&nbsp;
                <a href="<?php echo $linkMessageHref ?>" name="<?php echo $linkMessageName ?>">#</a>
            </p>
        </div>
    </div>
    <div class="message">
        <?php if($comment->getMessage()): ?>
            <p><?php echo htmlspecialchars($comment->getMessage()) ?></p>
        <?php else: ?>
            <p class="deleted">[cообщение удаленно]</p>
        <?php endif; ?>
    </div>
    <div class="manage">
        <?php $this->commentManageController->Show($comment); ?>
    </div>
</div>