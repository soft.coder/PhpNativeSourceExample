<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 23.10.13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */

require_once dirname(__FILE__)."/../models/GuestBookModel.php";


class GuestBookAddController {

    protected $isValidMessageData;

    function __construct(){
        $this->checkAddMessage();
        $this->showForm();
        $this->isValidMessageData = null;
    }
    protected function checkAddMessage(){
        if(isset($_REQUEST["doAdd"], $_REQUEST["message"])){
            $username = trim($_REQUEST["message"]["username"]);
            $messageText = trim($_REQUEST["message"]["message"]);
            if ( !empty($username) &&  !empty($messageText) && 
                    strlen($username) <= 100 && strlen($messageText) <= 1000){
                $this->isValidMessageData = true;
                $this->addMessage($username, $messageText);
            }
            else
                $this->isValidMessageData = false;
        }
    }

    protected function addMessage($username, $messageText){
        $model = new GuestBookModel();
        $messageAdded = new Message($username, $messageText);
        $model->SaveMessage($messageAdded);
    }
    protected function showForm(){
        include dirname(__FILE__) . "/../views/GuestBookAddView.php";
    }
}

$guestBookAddMsg = new GuestBookAddController();



