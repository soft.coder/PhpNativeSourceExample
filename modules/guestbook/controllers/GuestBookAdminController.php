<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 23.10.13
 * Time: 15:55
 * To change this template use File | Settings | File Templates.
 */



class GuestBookAdminController {
    protected $isAdmin, $replies;
    function __construct()
    {
        $this->checkAdmin();
        $this->checkRemove();
        $this->checkReply();
        
        $this->replies = $this->getReplies();
    }
    protected function checkAdmin(){
        if(isset($_SESSION["User"]) && $_SESSION["User"]->getRole() == "admin")
            $this->isAdmin = true;
        else
            $this->isAdmin = false;
    }
    protected function checkRemove(){
        if ( $this->isAdmin && isset($_REQUEST["doRemove"], $_REQUEST["parent_id"]) ){
            if(intval($_REQUEST["parent_id"])){
                $id = intval($_REQUEST["parent_id"]);
                $this->remove($id);
            }
        }
    }
    protected function checkReply(){
        if ( $this->isAdmin && isset($_REQUEST["doReply"], $_REQUEST["parent_id"], $_REQUEST["message"])){
            $msgText = trim($_REQUEST["message"]);
            $username = trim($_SESSION["User"]->getUsername());
            $parentId = intval($_REQUEST["parent_id"]);
            if(!empty($msgText) && !empty($username) && $parentId){
                $this->reply($username, $msgText, $parentId);
            }
        }
    }
    /**
     * @param string $username
     * @param string $message
     * @param int $parentId
     */
    protected function reply($username, $message, $parentId){
        $message = new Message($username, $message, 0, $parentId);
        $model = new GuestBookModel();
        $model->ReplyMessage($message);
    }
    /**
     * @param int $id
     */
    protected function remove($id){
        $model = new GuestBookModel();
        $model->RemoveMessage($id);
    }
    protected function getReplies()
    {
        $model = new GuestBookModel();
        return $model->LoadReplies();
    }
    function show($message){
        

        if ( !isset($_REQUEST["doCancel"]) && isset($_REQUEST["doReply"], $_REQUEST["parent_id"]) && 
                $_REQUEST["parent_id"] == $message->getId())
            $isReply = true;
        else
            $isReply = false;
        
        if ( isset($this->replies[$message->getId()]) )
        {
            $isReplied = true;
            $reply = $this->replies[$message->getId()];
            include dirname(__FILE__) . "/../views/GuestBookAdminRepliesView.php";
        }
        else
            $isReplied = false;
        
        if($this->isAdmin)
            include dirname(__FILE__) . "/../views/GuestBookAdminFormView.php";
    }
}
$guestBookAdmin = new GuestBookAdminController();
