<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 18.10.13
 * Time: 14:34
 * To change this template use File | Settings | File Templates.
 */
require_once dirname(__FILE__)."/../models/GuestBookModel.php";

class GuestBookShowMessagesController {
    function  showMessages()
    {
        include dirname(__FILE__) . "/../views/GuestBookShowMessagesView.php";
    }
    function getAllMessages()
    {
        $model = new GuestBookModel();
        return $model->LoadMessages();
    }
}

$controller = new GuestBookShowMessagesController();
$controller->showMessages();


