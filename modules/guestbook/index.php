<?php 
    require_once dirname(__FILE__) . "/../../controllers/PageController.php";
    $Page = new PageController();
    
    require_once dirname(__FILE__) . '/../menu/MenuModuleManager.php';
    $menuModuleManager = new MenuModuleManager();    
    
    $Page->ObStartEnable();
    $Page->setCharset("utf-8");
    $Page->setTitle("Гостевая книга");
    $Page->addCssStyle("css/guestbook.css");
    $Page->addCssStyle($menuModuleManager->getCssUrl());
    $Page->addCssStyle(AUTH_CSS);
    $Page->ShowHeader();
?>
<div>
<?php include AUTH_MENU_PATH; ?>
<?php $menuModuleManager->Show() ?>
</div>
<div class="my-guestbook">
    <?php require_once "controllers/GuestBookAddController.php" ?>
    <h3>Сообщения</h3>
    <?php require_once "controllers/GuestBookShowMessagesController.php" ?>
</div>
<?php $Page->ShowFooter(); ?>
