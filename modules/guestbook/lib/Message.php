<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 18.10.13
 * Time: 13:22
 * To change this template use File | Settings | File Templates.
 */

class Message {
    protected $id, $parent_id, $username, $message, $timeAdded;
    function __construct($username, $message, $id=0, $parent_id=null, $timeAdded=null){
        $this->username = $username;
        $this->message = $message;
        $this->id = $id;
        $this->timeAdded = $timeAdded;
        $this->parent_id = $parent_id;
    }

    function getUsername(){
        return $this->username;
    }
    function getMessage(){
        return $this->message;
    }
    function  getId(){
        return $this->id;
    }
    function  getTimeAdded(){
        return $this->timeAdded;
    }
    function  getParentId()
    {
        return $this->parent_id;
    }
}