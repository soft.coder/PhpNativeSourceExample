<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 18.10.13
 * Time: 13:41
 * To change this template use File | Settings | File Templates.
 */
require_once dirname(__FILE__)."/../lib/Message.php";

class GuestBookModel extends Model {
    const dbTableNameMessages = "messages";
    const dbTableNameReplies = "relplies";

    const TABLE_PREFIX = 'guestBook_';

    function InstallDb()
    {
        $tableNameMessages = self::TABLE_PREFIX . self::dbTableNameMessages;
        $query = <<<EOD
            CREATE TABLE $tableNameMessages(
                     id INT AUTO_INCREMENT PRIMARY KEY,
                     parent_id INT,
                     username CHAR(100),
                     message VARCHAR(1000),
                     time_added TIMESTAMP,
                     INDEX (parent_id),
                     FOREIGN KEY (parent_id)
                        REFERENCES $tableNameMessages(id)
                        ON UPDATE CASCADE
                        ON DELETE CASCADE
                 )ENGINE=InnoDb;
EOD;
        $this->Query($query);
    }
    function  SaveMessage(Message $msg)
    {
        $tableName = self::TABLE_PREFIX . self::dbTableNameMessages;
        $query = "INSERT INTO {$tableName} (username, message) VALUES ('{$msg->getUsername()}', '{$msg->getMessage()}')";
        $this->Query($query);
    }
    function LoadMessages()
    {
        $messages = array();
        $tableName = self::TABLE_PREFIX . self::dbTableNameMessages;
        $query = "SELECT id, username, message, DATE_FORMAT(time_added, '%T %d.%m.%Y') AS timeAdded
                    FROM {$tableName} WHERE parent_id IS NULL";
        $res = $this->Query($query);
        $num_row = $res->num_rows;
        for($i = 0; $i < $num_row; $i++)
        {
            $row = $res->fetch_assoc();
            $messages[] = new Message($row["username"], $row["message"], $row["id"], null, $row["timeAdded"]);
        }
        return $messages;
    }
    function LoadReplies()
    {
        $messages = array();
        $tableName = self::TABLE_PREFIX . self::dbTableNameMessages;
        $query = "SELECT id, username, message, parent_id, DATE_FORMAT(time_added, '%T %d.%m.%Y') AS timeAdded
                    FROM {$tableName} WHERE parent_id IS NOT NULL";
        $res = $this->Query($query);
        $num_row = $res->num_rows;
        for($i = 0; $i < $num_row; $i++)
        {
            $row = $res->fetch_assoc();
            $messages[$row["parent_id"]] = new Message($row["username"], $row["message"], $row["id"], $row["parent_id"], $row["timeAdded"]);
        }
        return $messages;
    }
    function  RemoveMessage($id, $removeAtFirstReply = true)
    {
        $num_affected_rows = $this->RemoveReply($id);
        if ( $num_affected_rows > 0 && $removeAtFirstReply == true)
            return $num_affected_rows;

        $tableName = self::TABLE_PREFIX . self::dbTableNameMessages;
        $query = "DELETE FROM $tableName WHERE id=$id";
        $db = $this->Query($query, false);
        return $db->affected_rows;
    }
    function  RemoveReply($parent_id)
    {
        $db = new mysqli($this->host, $this->username, $this->password, $this->dbName);
        $tableName = self::TABLE_PREFIX . self::dbTableNameMessages;
        $query = "DELETE FROM $tableName WHERE parent_id=$parent_id";
        $db = $this->Query($query, false);
        return $db->affected_rows;
    }
    function  ReplyMessage($msg)
    {
        $tableName = self::TABLE_PREFIX . self::dbTableNameMessages;
        $query = "INSERT INTO {$tableName} (username, message, parent_id)
                        VALUES ('{$msg->getUsername()}', '{$msg->getMessage()}', {$msg->getParentId()} )";
        $this->Query($query);
    }
}