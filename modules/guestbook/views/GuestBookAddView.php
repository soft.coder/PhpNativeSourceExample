<div class="guestbook-add">
<form method="post">
    <h3 class="title">Добавтье сообщение</h3>
    <p>Ваше имя:</p>
    <input class="username" name="message[username]" required="required" maxlength="100" type="text"/>
    <p>Ваше сообщение:</p>
    <textarea class="message" name="message[message]" required="required" maxlength="1000"></textarea>
    <p class="buttons"><input type="submit" name="doAdd" value="Добавить"/></p>
    <?php if($this->isValidMessageData === true): ?>
        <p class="success">Сообщение добавлено</p>
    <?php elseif ($this->isValidMessageData === false): ?>
        <p class="unsucces">Ошибка при добавлении сообщения</p>
    <?php endif; ?>
</form>
</div>