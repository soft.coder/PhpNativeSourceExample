<div class="reply-manage">
<form method="post">
    <?php if ($isReply == true && $isReplied == false ) : ?>
        <textarea name="message" maxlength="1000" class="message"></textarea>
    <?php endif; ?>
    <p class="buttons">
        <?php if ($isReplied == false): ?>
            <input type="submit" name="doReply" value="Ответить" />&nbsp;
        <?php endif; ?>
        <?php if($isReply == true && $isReplied == false) : ?>
            <input type="submit" name="doCancel" value="Отмена" />
        <?php else: ?>
            <input type="submit" name="doRemove" value="Удалить" />
        <?php endif; ?>
    </p>
    <input type="hidden" name="parent_id" value="<?php echo $message->getId(); ?>"/>
</form>
</div>