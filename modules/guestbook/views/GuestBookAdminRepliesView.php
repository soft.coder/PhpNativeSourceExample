<div class="reply-block">
    <div class="info">
        <div class="timeadd">
            <p><?php echo htmlspecialchars($reply->getTimeAdded()); ?></p>
        </div>
        <div class="username">
            <p><?php echo htmlspecialchars($reply->getUsername()) ?></p>
        </div>
    </div>
    <div class="reply">
        <p><?php echo htmlspecialchars($reply->getMessage()) ?></p>
    </div>
</div>