<?php
    require_once dirname(__FILE__) . "/../controllers/GuestBookAdminController.php";
?>
<div class="guestbook-messages">
<?php foreach($this->getAllMessages() as $message ) :?>
    <div class="message-block">
        <div class="info">
            <div class="timeadd">
                <p><?php echo htmlspecialchars($message->getTimeAdded()); ?></p>
            </div>
            <div class="username">
                <p><?php echo htmlspecialchars($message->getUsername()) ?></p>
            </div>
        </div>
        <div class="message">
            <p><?php echo htmlspecialchars($message->getMessage()) ?></p>
        </div>
        <?php $guestBookAdmin->show($message); ?>
    </div>
<?php endforeach; ?>
</div>