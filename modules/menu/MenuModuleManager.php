<?php

require_once dirname(__FILE__) . '/controllers/MenuAdminController.php';
require_once dirname(__FILE__) . '/controllers/MenuShowController.php';

class MenuModuleManager{
    
    protected $menuRootUrl, $menuShowUrl, $menuAdminUrl, $menuCssUrl;
    protected $menuAdminController, $menuShowController;
    function __construct($menuRootUrl = '/modules/menu') {
        $this->menuRootUrl = $menuRootUrl;
        $this->menuShowUrl = "$this->menuRootUrl/controllers/MenuShowController.php";
        $this->menuAdminUrl = "$this->menuRootUrl/controllers/MenuAdminController.php";
        $this->menuCssUrl = "$this->menuRootUrl/css/menu.css";
        $this->menuAdminController = new MenuAdminController();
        $this->menuShowController = new MenuShowController();
    }
    
    
    function Show(){
        $this->menuShowController->Show();
    }
    function ShowAdmin(){
        $this->menuAdminController->Show();
    }
    function getMenuRootUrl(){
        return $this->menuRootUrl;
    }
    function getMenuShowUrl(){
        return $this->menuShowUrl;
    }
    function getMenuAdminUrl(){
        return $this->menuAdminUrl;
    }
    function getCssUrl(){
        return $this->menuCssUrl;
    }
}

