<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuAdminController
 *
 * @author Serega
 */

require_once dirname(__FILE__) . '/../models/MenuModel.php';
require_once dirname(__FILE__) . '/../lib/MenuItem.php';
require_once dirname(__FILE__) . '/../../../controllers/PageController.php';

class MenuAdminController {
    /**
     * @var bool
     */
    protected $nowWritingChildMenuItem;
    function __construct() {
        $this->CheckAddRootMenuItem();
        $this->CheckAddChildMenuItem();
        $this->CheckRemoveMenuItem();
    }
    protected function CheckAddRootMenuItem(){
        if(isset($_REQUEST["doAddRoot"], $_REQUEST["txtName"], $_REQUEST["txtUrl"]) ){
            $name = htmlspecialchars(trim($_REQUEST["txtName"]));
            $url = htmlspecialchars(trim($_REQUEST["txtUrl"]));
            if(!empty($name) && !empty($url) && strlen($name) <= 100 && strlen($url) <= 1000){
                $menuItem = new MenuItem($name, $url);
                $menuModel = new MenuModel();
                $menuModel->InsertMenuItem($menuItem);
            }
        }
    }
    protected function CheckAddChildMenuItem(){
        if(isset($_REQUEST["doAddChild"], $_REQUEST["id"])){
            $this->nowWritingChildMenuItem = true;
            if(isset($_REQUEST["txtNameChild"], $_REQUEST["txtUrlChild"])){
                $this->nowWritingChildMenuItem = false;
                $menuModel = new MenuModel();
                $name = htmlspecialchars(trim($_REQUEST["txtNameChild"]));
                $url = urlencode(htmlspecialchars(trim($_REQUEST["txtUrlChild"])));
                $parentId = intval($_REQUEST["id"]);
                if(!empty($name) && !empty($url) && strlen($name) <= 100 
                        && strlen($url) <= 1000 && $parentId > 0){
                    $childMenuItem = new MenuItem($name, $url, null, $parentId);
                    $menuModel->InsertMenuItem($childMenuItem);
                }
            }
        }
    }
    protected function CheckRemoveMenuItem(){
        if(isset($_REQUEST["doRemove"], $_REQUEST["id"])){
            $id = intval($_REQUEST["id"]);
            if($id > 0){
                $menuModel = new MenuModel();
                $menuModel->DeleteMenuItem($id);
            }
        }
    }
    public function Show(){
		if (isset($_SESSION["User"]) && $_SESSION["User"]->getRole() == Role::ADMIN){
			$menuModel = new MenuModel();
			$menuItems = $menuModel->GetMenuItems();        
			include dirname(__FILE__) . '/../views/MenuAdminView.php';       
		}
    }

    protected function ShowMenuAdminViews(){
        include dirname(__FILE__) . '/../views/MenuAdminCommonFormView.php';
   }
    protected function ShowListOfMenuItems($menuItems, $parentId=null){
        if(count($menuItems) > 0 || ($this->nowWritingChildMenuItem === true && $_REQUEST["id"] == $parentId))
            include dirname(__FILE__) . '/../views/MenuAdminListOfMenuItemsView.php';
    }
    protected function ShowMenuItems($menuItems){
        for($i=0; $i < count($menuItems); $i++){
            $menuItem = $menuItems[$i];
            include dirname(__FILE__) . '/../views/MenuAdminMenuItemView.php';
        }
    }
    protected function ShowAddChildForm($parentId){
        if($this->nowWritingChildMenuItem === true && $_REQUEST["id"] == $parentId){
            include dirname(__FILE__) . '/../views/MenuAdminAddChildFormView.php';
        }
    }
}

?>
