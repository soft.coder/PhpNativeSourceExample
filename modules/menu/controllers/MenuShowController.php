<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuShowController
 *
 * @author Serega
 */
require_once dirname(__FILE__) . '/../models/MenuModel.php';
require_once dirname(__FILE__) . '/../lib/MenuItem.php';
require_once dirname(__FILE__) . '/../../../controllers/PageController.php';

class MenuShowController{
    public function Show(){
        $menuModel = new MenuModel();
        $menuItems = $menuModel->GetMenuItems();        
        include dirname(__FILE__) . '/../views/MenuShowView.php';       
    }
    protected function ShowListOfMenuItems($menuItems){
        if(count($menuItems) > 0)
            include dirname(__FILE__) . '/../views/MenuShowListOfMenuItemsView.php';
    }
    protected function ShowMenuItems($menuItems){
        for($i=0; $i < count($menuItems); $i++){
            if($i+1 == count($menuItems))
                $isLastOfCurrentLevel = true;
            $menuItem = $menuItems[$i];
            include dirname(__FILE__) . '/../views/MenuShowMenuItemView.php';
        }
    }
}
