<?php 
    require_once dirname(__FILE__) . "/../../controllers/PageController.php";
    $Page = new PageController();
    
    require_once 'MenuModuleManager.php';
    $menuModuleManager = new MenuModuleManager();    
    
    $Page->ObStartEnable();
    $Page->setCharset("utf-8");
    $Page->setTitle("Меню");
    $Page->addCssStyle($menuModuleManager->getCssUrl());
    $Page->addCssStyle(AUTH_CSS);
    $Page->ShowHeader();
?>
<div>
<?php include AUTH_MENU_PATH; ?>
<?php $menuModuleManager->Show() ?>
</div>
<div>
    <?php $menuModuleManager->ShowAdmin() ?>
</div>
<?php $Page->ShowFooter(); ?>
