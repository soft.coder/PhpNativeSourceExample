<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuItem
 *
 * @author svch
 */
class MenuItem {
    protected $id, $parent_id, $name, $url, $childs;
    /**
     * @param string $name
     * @param string $url
     * @param int $id 
     * @param int $parent_id
     * @param array $childs
     */
    function __construct($name, $url, $id=null, $parent_id=null, $childs=array()) {
        $this->id = is_null($id) || intval($id) <=0 ? null : intval($id);
        $this->parent_id = is_null($parent_id) || intval($parent_id) <= 0 ? null : intval($parent_id);
        $this->name = htmlspecialchars($name);
        $this->url = htmlspecialchars($url);
        $this->childs = $childs;
    }
    /**
     * 
     * @return int
     */
    function getId() {
        return $this->id;
    }
    /**
     * 
     * @return int
     */
    function getParentId(){
        return $this->parent_id;
    }
    /**
     * @return string
     */
    function getName(){
        return $this->name;
    }
    /**
     * @return string
     */
    function getUrl(){
        return $this->url;
    }
    /**
     * @return array
     */
    function getChilds(){
        return $this->childs;
    }
    function __toString() {
        $result = "{
    id:{$this->getId()};
    parent_id:{$this->getParentId()}; 
    name:{$this->getName()}; 
    url:{$this->getUrl()};
    childs:{$this->getChilds()}
}";
    return $result;
    }
}

?>
