<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once dirname(__FILE__) . '/../../../lib/Model.php';
require_once dirname(__FILE__) . '/../lib/MenuItem.php';

class MenuModel extends Model{
    protected $menuItemTableName;
    const TABLE_NAME = "menuitem";
    const TABLE_PREFIX = "menu_";
    function __construct() {
        parent::__construct();
        $this->menuItemTableName = self::TABLE_PREFIX . self::TABLE_NAME;
    }
    function InstallDb(){
        $query = "CREATE TABLE $this->menuItemTableName (
                    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
                    parent_id INT,
                    name CHAR(100) NOT NULL,
                    url VARCHAR(1000),
                    INDEX(parent_id),
                    FOREIGN KEY (parent_id)
                        REFERENCES $this->menuItemTableName(id)
                        ON UPDATE CASCADE
                        ON DELETE CASCADE
                ) ENGINE=InnoDb";
        $this->Query($query, false);
    }
    /**
     * @param int|null $parent_id
     * @param int $level
     * @return array
     */
    function GetMenuItems($parent_id=null, $level=1){
        if(is_null($parent_id))
            $wereClause = 'parent_id IS NULL';
        else
            $wereClause = "parent_id=$parent_id";
        
        $query = "SELECT id, name, url FROM $this->menuItemTableName
                        WHERE $wereClause ORDER BY id";
        $res = $this->Query($query);
        $menuItems = array();
        for($i=0; $i< $res->num_rows; $i++){
            $row = $res->fetch_assoc();
            
            $menuItemBranch  = $this->GetMenuItems($row["id"], $level+1);
            $menuItem = new MenuItem(stripslashes($row["name"]), 
                                        stripslashes($row["url"]),$row["id"], $parent_id, $menuItemBranch);
            $menuItems[] = $menuItem;
        }
        return $menuItems;
    }
    /**
     * 
     * @param MenuItem $menuItem
     * @param int $level
     * @return MenuItem|false
     */
    function InsertMenuItem(MenuItem $menuItem, $level=1){
        $parent_id = $menuItem->getParentId();
        $name = addslashes($menuItem->getName());
        $url = addslashes($menuItem->getUrl());
        if(is_null($parent_id)){
            $columnClause = '(name, url)';
            $valueClause = "('$name', '$url')";
        }
        else{
            $columnClause = '(parent_id, name, url)';
            $valueClause = "($parent_id, '$name', '$url')";
        }
        $query = "INSERT INTO $this->menuItemTableName $columnClause VALUES $valueClause";
        $db = $this->Query($query, false);
        if($db->insert_id != 0){
            
            $insertedChildMenuItems = array();
            foreach ($menuItem->getChilds() as $childMenuItem) {
                $insertedChildMenuItem = $this->InsertMenuItem($childMenuItem);
                if($insertedChildMenuItem != false)
                    $insertedChildMenuItems[] = $insertedChildMenuItem;
                else
                    throw new Exception("Insert child menuItem error: $childMenuItem");
            }            
            $insertedMenuItem = new MenuItem($menuItem->getName(), $menuItem->getUrl(), $db->insert_id,
                                                $menuItem->getParentId(), $insertedChildMenuItems);
            return $insertedMenuItem;
        }
        else
            return false;
    }
    /**
     * 
     * @param int $id
     * @return int
     */
    function DeleteMenuItem($id) {
        
        $query = "DELETE FROM $this->menuItemTableName WHERE id=$id";
        $db = $this->Query($query, false);
        if($db->affected_rows>0)
            return $db->affected_rows;
        else{
            $affectedRows = $this->DeleteChildsOfMenuItemManually ($id);
            $affectedRows += $this->DeleteMenuItem($id);
            return $affectedRows;
        }
    }
    /**
     * 
     * @param int $parent_id
     * @return int
     */
    function DeleteChildsOfMenuItemManually($parent_id){
        $querySelectChilds = "SELECT id FROM $this->menuItemTableName WHERE parent_id=$parent_id";
        $res = $this->Query($querySelectChilds);
        for($i=0; $i<$res->num_rows; $i++){
            $row = $res->fetch_assoc();
            $childId = intval($row["id"]);
            $affectedRowsFromChilds = $this->DeleteChildsOfMenuItemManually($childId);
            $queryDelete = "DELETE FROM $this->menuItemTableName WHERE id=$childId";
            $db = $this->Query($queryDelete, false);
            return $db->affected_rows + $affectedRowsFromChilds;
        }
        return 0;
    }

}

?>
