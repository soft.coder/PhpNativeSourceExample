<li>
    <form method="post">
        <span class="name" title="<?php echo $menuItem->getUrl(); ?>">
            <?php echo $menuItem->getName(); ?>
        </span>
        &nbsp;&nbsp;
        <?php if($this->nowWritingChildMenuItem !== true): ?>
        <span class="buttons">
            <input type="submit" name="doAddChild" value="Добавить" />
            <input type="submit" name="doRemove" value="Удалить" />
            <input type="hidden" name="id" value="<?php echo $menuItem->getId(); ?>" />
        </span>
        <?php endif; ?>
    </form>
    <?php $this->ShowListOfMenuItems($menuItem->getChilds(), $menuItem->getId()); ?>
</li>