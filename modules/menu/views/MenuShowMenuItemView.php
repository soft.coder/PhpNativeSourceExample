<li>
    <a href="<?php echo $menuItem->getUrl(); ?>" <?php if($isLastOfCurrentLevel == true) echo 'class="brd"'; ?>>
        <?php echo $menuItem->getName(); ?>
    </a>
    <?php $this->ShowListOfMenuItems($menuItem->getChilds(), $menuItem->getId()); ?>
</li>