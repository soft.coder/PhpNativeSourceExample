<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Serega
 * Date: 21.10.13
 * Time: 21:01
 * To change this template use File | Settings | File Templates.
 */
header("Content-type:text/html; charset=utf-8");

$CONFIG = parse_ini_file("config.ini", true);

//echo 'hello from prepend';

require_once dirname(__FILE__) . "/lib/Model.php";
require_once dirname(__FILE__) . "/modules/auth/init.php";
//echo $_SERVER['REQUEST_URI'];
//echo preg_match("/.php$/", $_SERVER['REQUEST_URI']);
if(preg_match("|.php$|", $_SERVER['REQUEST_URI']))
	require_once dirname(__FILE__) . $_SERVER['REQUEST_URI'];
else
	require_once dirname(__FILE__) . $_SERVER['REQUEST_URI'] . "index.php";

//header("Location: {$_SERVER['REQUEST_URI']}");