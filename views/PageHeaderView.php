<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=<?php echo $this->getCharset(); ?>">
    <title><?php echo $this->getTitle(); ?></title>
    <?php foreach($this->getCssStyles() as $href): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $href ?>"/>
    <?php endforeach; ?>
    <?php foreach($this->getJsScripts() as $src): ?>
    <script type="text/javascript" src="<?php echo $src ?>"></script>
    <?php endforeach; ?>
</head>
<body>